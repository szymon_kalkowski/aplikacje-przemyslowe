package com.example.zad2;

public interface SortAlgorithm {
    int[] sort(int[] numbers);
}
