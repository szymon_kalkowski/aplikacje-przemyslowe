package com.example.zad2;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Zad2Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Zad2Application.class, args);
		SortManager sortManager = context.getBean(SortManager.class);
		int[] arr = { 1, 5, 2, 4, 3 };
		sortManager.sort(arr);
		System.out.println(Arrays.toString(arr));
	}

}
