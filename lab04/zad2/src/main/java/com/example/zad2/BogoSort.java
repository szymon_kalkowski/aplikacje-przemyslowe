package com.example.zad2;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component("BogoSort")
public class BogoSort implements SortAlgorithm {
    public int[] sort(int[] arr) {
        while (!isSorted(arr)) {
            shuffleArray(arr);
        }
        return arr;
    }

    private boolean isSorted(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[i - 1]) {
                return false;
            }
        }
        return true;
    }

    private void shuffleArray(int[] arr) {
        int n = arr.length;
        Random rand = new Random();
        for (int i = n - 1; i > 0; i--) {
            int j = rand.nextInt(i + 1);
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }
}
