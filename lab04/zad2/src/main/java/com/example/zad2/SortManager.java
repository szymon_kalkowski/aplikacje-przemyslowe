package com.example.zad2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class SortManager {
    private final SortAlgorithm sortAlgorithm;

    public SortManager(@Qualifier("BogoSort") SortAlgorithm sortAlgorithm) {
        this.sortAlgorithm = sortAlgorithm;
    }

    public int[] sort(int[] arr) {
        return sortAlgorithm.sort(arr);
    }
}