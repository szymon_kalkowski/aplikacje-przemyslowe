package com.example.zad1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Zad1Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Zad1Application.class, args);
		MathApplication mathApplication = context.getBean(MathApplication.class);
		System.out.println(mathApplication.add(1, 2));
		System.out.println(mathApplication.substract(1, 2));
	}

}
