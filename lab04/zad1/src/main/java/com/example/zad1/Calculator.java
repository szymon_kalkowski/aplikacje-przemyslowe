package com.example.zad1;

import org.springframework.stereotype.Component;

@Component
public class Calculator {
    public Calculator() {
    }

    public int add(int a, int b) {
        return a + b;
    }

    public int substract(int a, int b) {
        return a - b;
    }
}
