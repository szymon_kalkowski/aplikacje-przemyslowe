package com.example.zad1;

import org.springframework.stereotype.Component;

@Component
class MathApplication {
    private final Calculator calculator;

    public MathApplication(Calculator calculator) {
        this.calculator = calculator;
    }

    public int add(int a, int b) {
        return calculator.add(a, b);
    }

    public int substract(int a, int b) {
        return calculator.substract(a, b);
    }
}