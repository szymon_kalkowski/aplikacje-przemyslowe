package com.example.zad3.osobny;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.zad3.domena.Person;

@Service
public class PersonService {
    private Person prezes;
    private Person wiceprezes;
    private Person sekretarka;

    public PersonService(@Qualifier("prezes") Person prezes, @Qualifier("wiceprezes") Person wiceprezes,
            @Qualifier("sekretarka") Person sekretarka) {
        this.prezes = prezes;
        this.wiceprezes = wiceprezes;
        this.sekretarka = sekretarka;
    }

    @Override
    public String toString() {
        return "PersonService{" +
                "prezes=" + prezes +
                ", wiceprezes=" + wiceprezes +
                ", sekretarka=" + sekretarka +
                '}';
    }
}
