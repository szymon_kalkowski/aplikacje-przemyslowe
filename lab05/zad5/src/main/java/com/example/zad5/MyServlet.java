package com.example.zad5;

import java.io.IOException;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {
    Logger logger = LoggerFactory.getLogger(MyServlet.class);

    protected void doAction(HttpServletRequest req, HttpServletResponse resp, String arg)
            throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        out.println("Witaj " + arg + "!!!");
        logger.info("Witaj " + arg + "!!!");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String arg = "GET request on /";
        logger.info(arg);
        doAction(req, resp, arg);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String arg = "POST request on /";
        logger.info(arg);
        doAction(req, resp, arg);
    }

}
