package com.example.zad5;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jakarta.servlet.http.HttpServlet;

@Configuration
public class WebConfiguration {
    @Bean
    ServletRegistrationBean<HttpServlet> servletRegistrationHelloBean() {

        return new ServletRegistrationBean<HttpServlet>(new MyServlet(), "/");
    }
}
