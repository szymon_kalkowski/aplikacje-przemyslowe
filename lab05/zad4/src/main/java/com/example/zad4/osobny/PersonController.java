package com.example.zad4.osobny;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.zad4.domena.Person;

@RestController
public class PersonController {
    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/persons")
    public List<Person> getAllPersons() {
        return personService.getAllPersons();
    }

    @PostMapping("/persons")
    public void addPerson(Person person) {
        personService.addPerson(person);
    }

    @PutMapping("/persons/{index}")
    public void updatePerson(@PathVariable("index") int index, Person person) {
        personService.updatePerson(index, person);
    }

    @DeleteMapping("/persons/{index}")
    public void removePerson(@PathVariable("index") int index) {
        personService.removePerson(index);
    }

}
