package com.example.zad4;

import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ImportResource;

import com.example.zad4.Zad4Application;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class Zad4Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Zad4Application.class, args);
		Stream.of(context.getBeanDefinitionNames()).map(context::getBean).forEach(System.out::println);
	}

}
