package com.example.zad4.osobny;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.zad4.domena.Person;

@Configuration
public class PersonServiceConfiguration {

    @Bean
    Person prezes() {
        return new Person("Chrystal", "Havoc", "chavocr@yahoo.com", "Mymm");
    }

    @Bean
    Person wiceprezes() {
        return new Person("Halley", "Gadaud", "hgadaud9@sohu.com", "Oyope");
    }

    @Bean
    Person sekretarka() {
        return new Person("Kirbie", "Wrettum", "kwrettumj@slideshare.net", "Browsetype");
    }

}
