package com.example.zad4.osobny;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

import com.example.zad4.domena.Person;

@Service
public class PersonService {
    private List<Person> personList;

    public PersonService(ConfigurableApplicationContext configurabelApplicationContext) {
        this.personList = new ArrayList<>(
                configurabelApplicationContext.getBeansOfType(Person.class).values().stream().toList());
    }

    public List<Person> getAllPersons() {
        return personList;
    }

    public void addPerson(Person person) {
        personList.add(person);
    }

    public void removePerson(int index) {
        personList.remove(index);
    }

    public Person getPerson(int index) {
        return personList.get(index);
    }

    public void updatePerson(int index, Person person) {
        personList.set(index, person);
    }

    @Override
    public String toString() {
        return "PersonService{" +
                "personList=" + personList +
                '}';
    }
}
