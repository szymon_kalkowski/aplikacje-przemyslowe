package com.example.validForm.Validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ZipCodeValidator implements ConstraintValidator<ZipCode, String> {
    public void initialize(ZipCode zipCode) {
    }

    @Override
    public boolean isValid(String zipCode, ConstraintValidatorContext context) {
        return zipCode.matches("^\\d{2}-\\d{3}$");
    }
}
