package com.example.validForm.Validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SalaryValidator implements ConstraintValidator<Salary, Double> {
    public void initialize(Salary salary) {
    }

    @Override
    public boolean isValid(Double salary, ConstraintValidatorContext context) {
        return salary >= 2000 && salary <= 3000;
    }
}
