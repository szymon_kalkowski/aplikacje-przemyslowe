package com.example.validForm.Controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.validForm.User;
import com.example.validForm.UserType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class UserController {
    public List<User> users;

    public UserController() {
        this.users = new ArrayList<User>(Arrays.asList(
                new User("admin", "Admin123", "admin@gmail.com", UserType.ADMIN)));
    }

    @GetMapping("/users")
    public String usersPage(Model model) {
        model.addAttribute("users", users);
        model.addAttribute("user", new User());
        model.addAttribute("userTypes", UserType.values());
        return "users";
    }

    @PostMapping("/users")
    public String processOrder(@Valid User user, Errors errors, Model model) {
        if (errors.hasErrors()) {
            model.addAttribute("users", users);
            model.addAttribute("userTypes", UserType.values());
            model.addAttribute("user", user);
            return "users";
        }
        this.users.add(user);
        log.info("User created: " + user);
        return "redirect:/users";
    }
}
