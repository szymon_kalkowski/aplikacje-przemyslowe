package com.example.validForm;

import lombok.Data;

// import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
// import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.example.validForm.Validators.Salary;
import com.example.validForm.Validators.ZipCode;

import javax.validation.constraints.AssertTrue;

@Data
public class Person {
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be start at least two characters")
    private String name;

    @NotNull(message = "Age is required")
    @Min(value = 0, message = "Age must be at least zero")
    private int age;

    @NotNull(message = "Zip code is required")
    // @Pattern(regexp = "^\\d{2}-\\d{3}$", message = "Zip code should be in format
    // xx-xxx")
    @ZipCode
    private String zipCode;

    @NotNull(message = "Salary is required")
    // @Min(value = 2000, message = "Salary must be at least 2000")
    // @Max(value = 3000, message = "Salary must be at most 3000")
    @Salary
    private double salary;

    @AssertTrue(message = "You must accept the terms")
    private boolean acceptTerms;
}
